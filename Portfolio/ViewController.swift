//
//  ViewController.swift
//  Portfolio
//
//  Created by Katarzyna Białas on 02/12/2018.
//  Copyright © 2018 Katarzyna Białas. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let items = [UIImage(named: "obraz1")!,
                 UIImage(named: "obraz2")!,
                 UIImage(named: "obraz3")!,
                 UIImage(named: "obraz4")!,
                 UIImage(named: "obraz5")!,
                 UIImage(named: "obraz6")!,
                 UIImage(named: "obraz7")!,
                 UIImage(named: "obraz8")!,
                 UIImage(named: "obraz9")!,
                 UIImage(named: "obraz10")!,
                 UIImage(named: "obraz11")!,
                 UIImage(named: "obraz12")! ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCellGrid
        cell.myPhoto.image = items[indexPath.item]
        return cell
    }
    
}

