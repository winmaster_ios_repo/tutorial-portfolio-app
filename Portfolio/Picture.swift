//
//  Picture.swift
//  Portfolio
//
//  Created by Katarzyna Białas on 02/12/2018.
//  Copyright © 2018 Katarzyna Białas. All rights reserved.
//

import Foundation
import UIKit

class Picture {
    var image : UIImage
    var title : String
    
    init(image: UIImage, title: String){
        self.image = image
        self.title = title
    }
}
