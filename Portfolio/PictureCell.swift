//
//  PictureCell.swift
//  Portfolio
//
//  Created by Katarzyna Białas on 02/12/2018.
//  Copyright © 2018 Katarzyna Białas. All rights reserved.
//

import UIKit

class PictureCell: UITableViewCell{
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var pictureTitleView: UILabel!
 
    func setPicture(pic: Picture){
        pictureImageView.image = pic.image
        pictureTitleView.text = pic.title
    }
}
