//
//  PicturesListScreen.swift
//  Portfolio
//
//  Created by Katarzyna Białas on 02/12/2018.
//  Copyright © 2018 Katarzyna Białas. All rights reserved.
//

import UIKit

class PicturesListScreen: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var pictures: [Picture] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pictures = createArray()
    }
    
    func createArray() -> [Picture] {
        
        var tmpPictures: [Picture] = []
        
        let pic1 = Picture(image: UIImage(named: "obraz1")!, title: "Tytuł 1")
        let pic2 = Picture(image: UIImage(named: "obraz2")!, title: "Tytuł 2")
        let pic3 = Picture(image: UIImage(named: "obraz3")!, title: "Tytuł 3")
        let pic4 = Picture(image: UIImage(named: "obraz4")!, title: "Tytuł 4")
        let pic5 = Picture(image: UIImage(named: "obraz5")!, title: "Tytuł 5")
        let pic6 = Picture(image: UIImage(named: "obraz6")!, title: "Tytuł 6")
        let pic7 = Picture(image: UIImage(named: "obraz7")!, title: "Tytuł 7")
        let pic8 = Picture(image: UIImage(named: "obraz8")!, title: "Tytuł 8")
        let pic9 = Picture(image: UIImage(named: "obraz9")!, title: "Tytuł 9")
        let pic10 = Picture(image: UIImage(named: "obraz10")!, title: "Tytuł 10")
        let pic11 = Picture(image: UIImage(named: "obraz11")!, title: "Tytuł 11")
        let pic12 = Picture(image: UIImage(named: "obraz12")!, title: "Tytuł 12")
        
        tmpPictures.append(pic1)
        tmpPictures.append(pic2)
        tmpPictures.append(pic3)
        tmpPictures.append(pic4)
        tmpPictures.append(pic5)
        tmpPictures.append(pic6)
        tmpPictures.append(pic7)
        tmpPictures.append(pic8)
        tmpPictures.append(pic9)
        tmpPictures.append(pic10)
        tmpPictures.append(pic11)
        tmpPictures.append(pic12)
        
        return tmpPictures
    }
}

extension PicturesListScreen: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return pictures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pic = pictures[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PictureCell") as! PictureCell
        
        cell.setPicture(pic: pic)
        return cell
    }
}
